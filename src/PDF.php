<?php

namespace Prepost;

use Dompdf;

class PDF
{
    protected $dompdf;

    protected $htmlData;

    protected $filename;

    public function __construct()
    {
        $this->dompdf = new Dompdf\Dompdf();

        $options = $this->dompdf->getOptions();
        $options->setDefaultPaperSize( 'letter' );
        $options->setDefaultPaperOrientation( "portrait" );
        $options->setTempDir( PREPOST__PLUGIN_TEMP_DIR );
        $options->setChroot( PREPOST__PLUGIN_DIR );

        $options->setIsHtml5ParserEnabled( true );
        $options->setIsRemoteEnabled( false );
        $options->setIsPhpEnabled( false );
        $options->setIsFontSubsettingEnabled( false );

        $this->dompdf->setOptions( $options );

        $this->dompdf->setBaseHost( PREPOST__PLUGIN_SITE_URL );
        $this->dompdf->setBasePath( PREPOST__PLUGIN_SITE_URL );

        $this->dompdf->add_info( 'Creater', 'Ovenvalets' );
        $this->dompdf->add_info( 'Author', 'Ovenvalets' );
        $this->dompdf->add_info( 'Subject', 'Van 4 Booking Response' );
    }

    public function processPrepost( $data = [] )
    {
        try {
            $customername = trim( $data['customer'] );

            $date      = new \DateTime();
            $timestamp = $date->format( "U" );
            $filename  = strtolower( str_replace( " ", "_", $customername ) ) . "_" . $timestamp . ".pdf";

            $this->filename = $filename;

            $htmlcontent = file_get_contents( PREPOST__PLUGIN_TEMPLATES_DIR . '/pdf.html' );

            preg_match_all('~{{(.*?)}}~s', $htmlcontent, $matches);

            $data['image'] = PREPOST__PLUGIN_DIR . 'assets/images/oven-cleaning-valets-sydney-header.jpg';

            foreach($matches[0] as $match) {
                $key = str_replace(['{{', '}}'], '', $match);
                $htmlcontent = str_ireplace($match, $data[$key], $htmlcontent);
            }

            $this->dompdf->loadHtml( $htmlcontent );
        } catch ( Exception $e ) {
            throw $e;
        }
    }

    public function generatePDF()
    {
        try {
            $this->dompdf->render();

            $this->dompdf->stream( $this->filename, [
                'compress'   => 1,
                'Attachment' => 0
            ] );
        } catch ( Exception $e ) {
            throw $e;
        }
    }

    public function download()
    {
        try {
            $this->dompdf->render();

            $this->dompdf->stream( $this->filename, [
                'compress'   => 1,
                'Attachment' => 1
            ] );
        } catch ( Exception $e ) {
            throw $e;
        }
    }
}
