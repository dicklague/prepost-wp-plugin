<?php
namespace Prepost;

class PrepostPDF
{
    protected $slug = "prepostieghae6a";

    public function __construct()
    {
        add_action( 'init', [$this, 'registerRoute'] );
        add_filter( 'query_vars', [$this, 'queryVars'] );
        add_action( 'template_include', [$this, 'renderPDF'] );
    }

    public function registerRoute()
    {
        add_rewrite_rule( $this->slug . '/([a-z]+)[/]?$', 'index.php?' . $this->slug . '=$matches[1]', 'top' );
        flush_rewrite_rules();
    }

    public function queryVars( $query_vars )
    {
        $query_vars[] = $this->slug;
        return $query_vars;
    }

    public function renderPDF( $template )
    {
        if ( get_query_var( $this->slug ) == false || get_query_var( $this->slug ) == '' ) {
            return $template;
        }

        $data = [
            'customer'       => self::parseInput( 'customer' ),
            'email'          => self::parseInput( 'email' ),
            'phone'          => self::parseInput( 'phone' ),
            'address'        => self::parseInput( 'address' ),
            'appliance'      => self::parseInput( 'appliance' ),
            'condition'      => self::parseInput( 'condition' ),
            'need_attention' => self::parseInput( 'need_attention' ),
            'amount'         => self::parseInput( 'amount', 'formatCurrency' ),
            'cleaningdate'   => self::parseInput( 'cleaningdate' )
        ];

        try {
            $pdf = new PDF();
            $pdf->processPrepost( $data );
            $pdf->download();
        } catch ( Exception $e ) {
            Debug::dd( $e );
        }
    }

    protected static function parseInput( $key, $type = 'string' )
    {
        $value = isset( $_REQUEST[$key] ) ? $_REQUEST[$key] : null;

        if ( $type === 'string' ) {
            $value = trim( $value );
        }

        if ( $type === 'formatCurrency' ) {

        }

        switch ( $type ) {
            case 'formatCurrency':
                $value = (double) str_replace( ['$', '', ','], '', $value );
                $value = '$' . number_format( $value, 2, '.', ',' );
            default:
                $value = trim( $value );
        }

        return $value;
    }
}
