<?php
namespace Prepost;

use WP_Error;
use WP_REST_Controller;
use WP_REST_Response;
use WP_REST_Server;

class PrepostRestApi extends WP_REST_Controller
{
    protected $version = "v1";

    protected $route = "prepost";

    protected $service = "booking";

    public function __construct()
    {
        add_action( 'rest_api_init', [$this, 'registerRoutes'] );
    }

    public function registerRoutes()
    {
        $route_namespace = $this->route . '/' . $this->version;
        $base_route      = '/' . $this->service;

        register_rest_route( $route_namespace, $base_route . '/(?P<id>[a-zA-Z0-9-_]+)', [
            [
                'methods'             => WP_REST_Server::READABLE,
                'callback'            => [ $this, 'getRecords' ],
                'permission_callback' => [ $this, 'checkPermission' ],
                'args'                => [
                    'context' => [
                        'default' => 'view'
                    ]
                ]
            ]
        ]);
    }

    /**
     * Check if a given request has access to get items
     *
     * @param WP_REST_Request $request Full data about the request.
     * @return WP_Error|bool
     */
    public function checkPermission( $request )
    {
        return true;
    }

    /**
     * Get a collection of items
     *
     * @param WP_REST_Request $request Full data about the request.
     * @return WP_Error|WP_REST_Response
     */
    public function getRecords( $request )
    {
        try {
            $params = $request->get_params();

            $config = new Config();

            $spredsheet = $config->getSpreadsheetById( $params['id'] );

            if ( empty( $spredsheet ) ) {
                throw new Exception( 'The selected bookig response was not found.' );
            }

            $prepost = new Spreadsheet();
            $prepost->setSpreadsheet( $spredsheet['spreadsheetId'] );
            $prepost->setWorkSheet( $spredsheet['worksheet_name'] );
            $prepost->setMapping( $spredsheet['mapping'] );

            $data                   = $prepost->getRecords();
            $total_quotation_amount = $prepost->getTotalQuotation();

            $response = [
                'status'             => true,
                'found'              => count( $data ),
                'total_quoted_amount' => $total_quotation_amount,
                'records'            => $data
            ];

            return new WP_REST_Response( $response, 200 );
        } catch ( Exception $e ) {
            return new WP_Error( 'REQUEST_NOT_FOUND', $e->getMessage(), [ 'status' => 404 ] );
        }
    }
}
