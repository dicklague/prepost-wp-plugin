<?php
namespace Prepost;

use Google;

class Spreadsheet
{
    protected $client;

    protected $spreadsheet;

    protected $worksheet;

    protected $service;

    protected $range = 'A1:P';

    protected $entries = [];

    protected $totalQuotation = 0;

    protected $mapping = [];

    public function __construct()
    {
        try {
            $this->client = new Google\Client();

            $this->client->setAuthConfig(
                PREPOST__PLUGIN_DATA_DIR . 'prepost-application-49261937134b.json'
            );

            $this->client->useApplicationDefaultCredentials();

            $this->client->setScopes( [
                Google\Service\Sheets::SPREADSHEETS_READONLY
            ] );

            $this->service = new Google\Service\Sheets( $this->client );
        } catch ( Google\Exception $exception ) {
            $e     = json_decode( $exception->getMessage() );
            $error = (array) $e->error;
            throw new Exception( sprintf( '[%s] %s', $error['status'], $error['message'] ) );
        } catch ( Exception $e ) {
            throw $e;
        }
    }

    public function setSpreadsheet( $spreadsheetId )
    {
        $this->spreadsheet = $spreadsheetId;
    }

    public function setWorkSheet( $sheetName )
    {
        $this->worksheet = $sheetName;
    }

    public function setMapping( $mapping ) {
        $this->mapping = $mapping;
    }

    public function getRecords()
    {
        try {
            $range    = $this->worksheet . '!' . $this->range;
            $response = $this->service->spreadsheets_values->get( $this->spreadsheet, $range );
            $entries  = $response->getValues();

            $this->entries = $this->parseEntries( $entries );

            return $this->entries;
        } catch ( Google\Exception $exception ) {
            $e     = json_decode( $exception->getMessage() );
            $error = (array) $e->error;
            throw new Exception( sprintf( '[%s] %s', $error['status'], $error['message'] ) );
        } catch ( Exception $e ) {
            throw $e;
        }

    }

    public function parseEntries( $entries = [] )
    {
        try {
            $total_quotation = 0;

            $columnHeaders = array_slice( $entries, 0, 1 );
            $rows          = array_slice( $entries, 1 );

            $columnHeaders = $this->fieldMapping($columnHeaders[0]);

            $records = [];

            $rowId = 1000001;

            foreach ( $rows as $row ) {
                $tablerow = [];

                $tablerow['id'] = $rowId;

                foreach ( $columnHeaders as $index => $header ) {

                    if ( !empty( $header ) ) {
                        $key                        = preg_replace( '/[^A-Za-z0-9_\-]/', '', $header );
                        $tablerow[strtolower( $key )] = isset( $row[$index] ) ? $row[$index] : '';
                    }

                }

                if ( !empty( $tablerow['firstlastname'] ) ) {
                    array_push( $records, $tablerow );

                    $total_quotation += (double) preg_replace( '/[^\d.]/', '', $tablerow['pricequoted'] );

                    $rowId++;
                }

            }

            $this->totalQuotation = (double) $total_quotation;

            return $records;
        } catch ( Exception $e ) {
            return [];
        }

    }

    public function fieldMapping( $fields = [] ) {
        $mapping = $this->mapping;

        if( empty($mapping) ) {
            return $fields;
        }

        $headers = [];

        foreach($mapping as $key => $field) {

            $col = array_filter($fields, function($item) use ($field) {
                $item = preg_replace( '/[^A-Za-z0-9_\-]/', '', $item );
                return strtolower($item) === strtolower($field);
            }, ARRAY_FILTER_USE_BOTH);

            if( empty($col) ) {
                continue;
            }

            $index = array_key_last($col);

            $headers[$index] = $key;
        }

        if( empty($headers) ) {
            return $fields;
        }

        ksort($headers);

        return $headers;
    }

    public function getTotalQuotation()
    {
        return (double) $this->totalQuotation;
    }

}
