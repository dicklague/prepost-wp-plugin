<?php
namespace Prepost;

final class Config
{
    protected $spreadsheets = [
        1  => [
            "spreadsheetId"  => "0Atv-nDhu9Bu1dGVFQWliQkh6RWswbnNxYURaVDNCb2c",
            "worksheet_name" => "Form Responses",
            "title"          => "Van 1 Booking Records",
            "label"          => "Booking Response 1",
            "mapping"        => [
                "timestamp"         => "timestamp",
                "bcleaningdate"     => "date",
                "worktime"          => "worktime",
                "canres"            => "canres",
                "firstlastname"     => "customername",
                "address"           => "customeraddress",
                "postcode"          => "postcode",
                "appliancetype"     => "appliancetype",
                "pricequoted"       => "pricequoted",
                "totalprice"        => "totalprice",
                "contactnumber"     => "contactnumber",
                "email"             => "emailaddress"
            ]
        ],
        2  => [
            "spreadsheetId"  => "1DFMEi4RdaHiEQQzZXuRdsOImQWZTtTVfhR76jCq_QsQ",
            "worksheet_name" => "Form Responses",
            "title"          => "Van 2 Booking Response",
            "label"          => "Booking Response 2",
            "mapping"        => [
                "timestamp"         => "timestamp",
                "bcleaningdate"     => "date",
                "worktime"          => "worktime",
                "canres"            => "canres",
                "firstlastname"     => "customername",
                "address"           => "customeraddress",
                "postcode"          => "postcode",
                "appliancetype"     => "appliancetype",
                "pricequoted"       => "pricequoted",
                "totalprice"        => "totalprice",
                "contactnumber"     => "contactnumber",
                "email"             => "emailaddress"
            ]
        ],
        4  => [
            "spreadsheetId"  => "1-hzrRCZrVKmbcEQjRvuFJgL2l5GvWUMNqdV4IxBduww",
            "worksheet_name" => "Form Responses 1",
            "title"          => "Van 4 Booking Records",
            "label"          => "Booking Response 4",
            "mapping"        => [
                "timestamp"         => "timestamp",
                "bcleaningdate"     => "bcleaningdate",
                "worktime"          => "worktime",
                "canres"            => "canres",
                "firstlastname"     => "firstlastname",
                "address"           => "address",
                "postcode"          => "postcode",
                "appliancetype"     => "appliancetype",
                "pricequoted"       => "pricequoted",
                "totalprice"        => "totalprice",
                "contactnumber"     => "contactnumber",
                "email"             => "email"
            ]
        ],
        55 => [
            "spreadsheetId"  => "143g0LpN94DgJXhPegyfVZFxipw6j73kzhvbBd4scicE",
            "worksheet_name" => "Form Responses",
            "title"          => "Van 5 Booking Response",
            "label"          => "Booking Response 5",
            "mapping"        => [
                "timestamp"         => "",
                "bcleaningdate"     => "cleaningdate",
                "worktime"          => "worktime",
                "canres"            => "canres",
                "firstlastname"     => "cutomername",
                "address"           => "address",
                "postcode"          => "postcode",
                "appliancetype"     => "appliancetype",
                "pricequoted"       => "pricequoted",
                "totalprice"        => "totalprice",
                "contactnumber"     => "mobile",
                "email"             => "emailaddress"
            ]
        ],
        5  => [
            "spreadsheetId"  => "192lHxJ0_19mcGPnDIZS7i50NbodYm-m5MMIKdaNyD-w",
            "worksheet_name" => "Booking Confirmation Form",
            "title"          => "Van 5 Booking Records",
            "label"          => "Booking Response 5",
            "mapping"        => [
                "timestamp"         => "timestamp",
                "bcleaningdate"     => "bcleaningdate",
                "worktime"          => "worktime",
                "canres"            => "canres",
                "firstlastname"     => "firstlastname",
                "address"           => "address",
                "postcode"          => "postcode",
                "appliancetype"     => "appliancetype",
                "pricequoted"       => "pricequoted",
                "totalprice"        => "totalprice",
                "contactnumber"     => "contactnumber",
                "email"             => "email"
            ]
        ],
        7  => [
            "spreadsheetId"  => "1OrGP6wY1ndQhdwFaHRHLVe4MqFG-fzlo-LfFBuqkVa8",
            "worksheet_name" => "Form Responses",
            "title"          => "Van 7 Booking Response",
            "label"          => "Booking Response 7",
            "mapping"        => [
                "timestamp"         => "",
                "bcleaningdate"     => "cleaningdate",
                "worktime"          => "worktime",
                "canres"            => "canres",
                "firstlastname"     => "name",
                "address"           => "address5r",
                "postcode"          => "postcode",
                "appliancetype"     => "appliancetype",
                "pricequoted"       => "pricequoted",
                "totalprice"        => "totalprice",
                "contactnumber"     => "contactno",
                "email"             => "emailaddress"
            ]
        ]
    ];

    public function getSpreadsheets()
    {
        return $this->spreadsheets;
    }

    public function getSpreadsheet( $van )
    {
        return !!$this->spreadsheets[$van] ? $this->spreadsheets[$van] : null;
    }

    public function getSpreadsheetById( $spreadsheetID )
    {
        return array_values( array_filter( $this->spreadsheets, function ( $item ) use ( $spreadsheetID ) {
            return $item['spreadsheetId'] === $spreadsheetID;
        } ) )[0];
    }
}
