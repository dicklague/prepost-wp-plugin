<?php
namespace Prepost;

final class Setup
{
    /**
     * Run only once after plugin is activated
     * @docs https://developer.wordpress.org/reference/functions/register_activation_hook/
     */
    public static function activation()
    {

        if ( !current_user_can( 'activate_plugins' ) ) {
            return;
        }

        add_option( 'Activated_' . PREPOST__PLUGIN_NAME, true );

        // Clear the permalinks
        flush_rewrite_rules();
    }

    /**
     * Run only once after plugin is deactivated
     * @docs https://developer.wordpress.org/reference/functions/register_deactivation_hook/
     */
    public static function deactivation()
    {

        if ( !current_user_can( 'activate_plugins' ) ) {
            return;
        }

        delete_option( 'Activated_' . PREPOST__PLUGIN_NAME );

        // Clear the permalinks
        flush_rewrite_rules();
    }

    /**
     * Run only once after plugin is uninstalled
     * @docs https://developer.wordpress.org/reference/functions/register_uninstall_hook/
     */
    public static function uninstall()
    {

        if ( !current_user_can( 'activate_plugins' ) ) {
            return;
        }

        // If uninstall not called from WordPress, then exit.
        if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
            return;
        }

    }

}
