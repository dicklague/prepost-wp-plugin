<?php
namespace Prepost;

class Shortcode
{
    public function __construct()
    {
        add_action( 'init', [$this, 'setResources'] );
        add_shortcode( 'prepost_booking_response', [$this, 'createShortcode'] );
    }

    public function setResources()
    {
        wp_register_script( "prepost-shortcode-axiosjs", PREPOST__PLUGIN_URL_ASSETS . 'axios/axios.min.js', [], "1.0", true );
        wp_register_script( "prepost-shortcode-accountingjs", PREPOST__PLUGIN_URL_ASSETS . 'js/accounting.min.js', [], "1.0", true );
        wp_register_script( "prepost-shortcode-appjs", PREPOST__PLUGIN_URL_ASSETS . 'js/prepost-plugin.min.js', ['jquery', 'prepost-shortcode-axiosjs', 'prepost-shortcode-accountingjs'], PREPOST__VERSION . '.1', true );
        wp_register_style( "prepost-shortcode-css", PREPOST__PLUGIN_URL_ASSETS . 'css/prepost-plugin.min.css', [], PREPOST__VERSION . '.1', "all" );
    }

    public function createShortcode( $attributes = [] )
    {
        extract( shortcode_atts( [
            'spreadsheet' => '',
            'title'       => ''
        ], $attributes ) );

        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( "prepost-shortcode-axiosjs", [], '1.0', true );
        wp_enqueue_script( "prepost-shortcode-accountingjs", [], '1.0', true );
        wp_enqueue_script( "prepost-shortcode-appjs", ['jquery', 'prepost-shortcode-axiosjs', 'prepost-shortcode-accountingjs'], PREPOST__VERSION . '.1', true );
        wp_enqueue_style( "prepost-shortcode-css" );

        wp_localize_script( 'prepost-shortcode-appjs', 'wp_prepost_rest_api', array(
            'base_url' => esc_url_raw( get_site_url() ),
            'base_api_url'  => esc_url_raw( rest_url() ),
            'nonce' => wp_create_nonce( 'wp_rest' )
        ) );

        $header = file_get_contents( PREPOST__PLUGIN_TEMPLATES_DIR . 'layout/header.php' );
        $main   = file_get_contents( PREPOST__PLUGIN_TEMPLATES_DIR . 'layout/main.php' );
        $footer = file_get_contents( PREPOST__PLUGIN_TEMPLATES_DIR . 'layout/footer.php' );

        $layout = sprintf( '<div id="__prepost-mainpage">%s %s %s</div><div id="__prepost-extractpage"></div>', $header, $main, $footer );

        return sprintf( '<div id="__prepost-plugin-app" data-spreadsheet="%s" data-title="%s">%s</div>', trim( $spreadsheet ), trim( $title ), $layout );
    }
}
