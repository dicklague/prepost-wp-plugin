<footer id="__prepost-plugin-footer" class="footer margin-top-auto padding-top-3 padding-bottom-3">
    <div class="prepost-container">
        <nav aria-label="Prepost pagination">
            <ul class="prepost-pagination flex-justify-content-end" id="__prepost-pagination">
                <li class="pagination-item disabled">
                    <a class="pagination-link" data-page="first">First</a>
                </li>
                <li class="pagination-item disabled">
                    <a class="pagination-link" data-page="previous">Previous</a>
                </li>
                <li class="pagination-item disabled">
                    <span class="pagination-link padding-left-4 padding-right-4" id="__prepost-pagination-counter">page 0 of 0</span>
                </li>
                <li class="pagination-item">
                    <a class="pagination-link" href="#page=2" data-page="next">Next</a>
                </li>
                <li class="pagination-item">
                    <a class="pagination-link" href="#page=10" data-page="last">Last</a>
                </li>
            </ul>
        </nav>
    </div>
</footer>