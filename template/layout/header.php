<header id="__prepost-plugin-header">
    <nav class="navbar border-bottom padding-bottom-1">
        <div class="prepost-container prepost-display-flex flex-justify-content-end">
            <div id="__prepost-search" class="prepost-search-group margin-bottom-0 width-50 width-sm-100">
                <select class="prepost-select width-25" id="__prepost-search-key" aria-label="Filter By">
                    <option selected>Filter By</option>
                    <option value="firstlastname">Customer Name</option>
                    <option value="email">Email Address</option>
                    <option value="contactnumber">Phone Number</option>
                    <option value="address">Location</option>
                    <option value="appliancetype">Appliance</option>
                    <option value="all">Display All</option>
                </select>
                <input id="__prepost-search-input" type="text" class="prepost-input" placeholder="Search term..." aria-label="Search term..." aria-describedby="__prepost-search-button">
                <button type="button" class="prepost-btn" id="__prepost-search-button">
                    Search
                </button>
            </div>
        </div>
    </nav>

    <div class="nav-scroller box-shadow">
        <div class="prepost-display-flex padding-2 text-uppercase font-16">
            <div class="font-bold d-none d-sm-block text-start prepost-plugin-page-title width-100">Booking Response</div>
            <div class="prepost-flex-shrink-0 prepost-flex-grow-1 text-end">
            Total Quotation: <span id="__prepost-total-quotation-amount" class="font-bolder padding-left-2">$0.00</span></div>
        </div>
    </div>
</header>