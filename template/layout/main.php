<main id="__prepost-plugin-main" class="prepost-flex-shrink-0 margin-left-2 margin-right-2 padding-top-3 padding-bottom-3">
    <div class="prepost-table-responsive">
        <table id="__prepost-plugin-listview" class="prepost-table prepost-table-striped prepost-table-hover">
            <thead>
                <tr>
                    <th scope="col" nowrap="nowrap">Action</th>
                    <th scope="col" nowrap="nowrap">Cleaning Date</th>
                    <th scope="col" nowrap="nowrap">Customer</th>
                    <th scope="col" nowrap="nowrap">Email</th>
                    <th scope="col" nowrap="nowrap">Phone</th>
                    <th scope="col" nowrap="nowrap" style="max-width: 100px;">Location</th>
                    <th scope="col" nowrap="nowrap" style="max-width: 100px;">Appliance(s)</th>
                    <th scope="col" nowrap="nowrap">Quoted Price</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</main>