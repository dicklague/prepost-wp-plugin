<?php
/**
 * @package Prepost
 * @version 1.0.1
 *
 * Plugin Name: Ovenvalets Prepost Records
 * Description: Extract ovenvalets prepost records from the google spreadsheets.
 * Version: 1.0.1
 * Author: Dick Lague <laguedick0589@gmail.com>
 * License: GPLv2 or later
 */

/**
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright 2005-2015 dicklague
 */

if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define( 'PREPOST__VERSION', '1.0.1' );
define( 'PREPOST__TEXTDOMAIN', 'prepost' );
define( 'PREPOST__PLUGIN_NAME', 'Prepost');
define( 'PREPOST__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'PREPOST__PLUGIN_URL_ASSETS', plugin_dir_url( __FILE__ ) . 'assets/' );
define( 'PREPOST__PLUGIN_DATA_DIR', plugin_dir_path( __FILE__ ) . 'data/' );
define( 'PREPOST__PLUGIN_TEMPLATES_DIR', plugin_dir_path( __FILE__ ) . 'template/' );
define( 'PREPOST__PLUGIN_FILE', __FILE__ );
define( 'PREPOST__PLUGIN_TEMP_DIR', plugin_dir_path( __FILE__ ) . 'tmp');
define( 'PREPOST__PLUGIN_SITE_URL', get_site_url());
define( 'PREPOST__MIN_PHP_VERSION', '5.6' );
define( 'PREPOST__WP_VERSION', '5.3' );

require_once PREPOST__PLUGIN_DIR . 'lib/autoload.php';

register_activation_hook( __FILE__, ['Prepost\Setup', 'activation'] );
register_deactivation_hook( __FILE__, ['Prepost\Setup', 'deactivation'] );
register_uninstall_hook( __FILE__, ['Prepost\Setup', 'uninstall'] );

new Prepost\PrepostRestApi();
new Prepost\Shortcode();
new Prepost\PrepostPDF();