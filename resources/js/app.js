(function ($, window, undefined) {
  const PrepostEvent = (function () {
    let listeners = [];

    return {
      subscribe: function (event, callback) {
        if (typeof callback == "function") {
          listeners[event] = listeners[event] || [];
          listeners[event].push(callback);
        }
      },
      emit: function (event, payload) {
        if (!!listeners) {
          if (!!listeners[event]) {
            listeners[event].forEach(function (fn) {
              if (typeof fn === "function") {
                fn(payload);
              }
            });
          }
        }
      },
    };
  })();

  const PrepostConfig = (function () {
    let options = [];

    return {
      config: function (config) {
        options = Object.assign({}, options, config);
      },
      getConfig: function () {
        return options;
      },
      set: function (key, value) {
        options[key] = value;
      },
      unset: function (key) {
        if (!!options[key]) {
          delete options[key];
        }

        return true;
      },
      get: function (key) {
        return options[key] || null;
      },
    };
  })();

  const PrepostRecord = (function () {
    function PrepostRecord(options) {
      this.records = [];

      this.options = options;

      this.options.rows_per_page = options.rows_per_page || 10;

      this.total_quotation = 0;

      this.total_records_found = 0;
    }

    PrepostRecord.prototype = {
      fetch: async (options) => {
        let data = options;

        let url = `${PrepostConfig.get("base_api_url")}prepost/v1/booking/${
          data.spreadsheet
        }`;

        let nonce = `${PrepostConfig.get("nonce")}`;

        let xhr = await axios.get(url, {
          headers: { "X-WP-Nonce": nonce },
        });

        if (!!xhr.data.status) {
          this.records = xhr.data.records;

          this.total_records_found = xhr.data.found;

          this.total_quotation = xhr.data.total_quoted_amount;
        }
      },
      getList: () => {
        return !!this.records ? this.records.map((item) => item) : [];
      },
      getTotalQuotation: () => {
        return this.total_quotation || 0;
      },
      fetchRecords: function (search, page, perpage) {
        let __list = this.getList();

        search = Object.assign(
          {},
          {
            key: null,
            keyword: null,
          },
          search
        );

        page = page || 1;

        perpage = perpage || this.options.rows_per_page;

        paginate_start_row = page > 1 ? (page - 1) * perpage : 0;
        paginate_end_row = perpage;

        if (!!search.key && !!search.keyword) {
          if (!["Filter By", "all"].includes(search.key)) {
            __list = __list.filter((item) => {
              return (item[search.key] || "")
                .toLowerCase()
                .includes(search.keyword.toLowerCase());
            });
          }
        }

        let _total_rows = __list.length;
        let _total_pages = Math.ceil(_total_rows / perpage);

        __list = __list.splice(paginate_start_row, paginate_end_row);

        return {
          records: __list,
          page: page,
          perpage: perpage,
          rows: _total_rows,
          pages: _total_pages,
        };
      },
    };

    return PrepostRecord;
  })();

  const PrepostListview = (function () {
    function PrepostListview($container) {
      this.$container = $container;
      this.__bindListener();
    }

    PrepostListview.prototype = {
      setList: function (listdata) {
        this.list = listdata.map((item) => item);
      },
      row: function (item) {
        const i = {
          bcleaningdate: item.bcleaningdate ? `${item.bcleaningdate}` : "",
          firstlastname: item.firstlastname ? `${item.firstlastname}` : null,
          email: item.email ? `${item.email}` : null,
          contactnumber: item.contactnumber ? `${item.contactnumber}` : null,
          address: item.address
            ? `${item.address + (item.postcode || null)}`
            : null,
          appliancetype: item.appliancetype ? `${item.appliancetype}` : null,
          pricequoted: item.pricequoted || "$0.00",
        };

        let $tr = $('<tr scope="row" />');

        let $btn = $("<button />");
        $btn.addClass(
          "prepost-btn prepost-btn-primary-outline prepost-btn-extract"
        );
        $btn.attr("type", "button");
        $btn.attr("name", "extract");
        $btn.attr("id", `"__prepost-item-btn-${item.id}"`);
        $btn.val(item.id);
        $btn.html("Extract");

        let $tdBtn = $("<td />");
        $tdBtn.addClass("align-baseline");
        $tdBtn.attr("scope", "row");
        $tdBtn.attr("nowrap", "nowrap");
        $tdBtn.append($btn);

        $tr.append($tdBtn);

        Object.keys(i).forEach((j) => {
          let $td = $("<td />").html(i[j]);
          $td.addClass("align-baseline");

          if (["address", "appliancetype"].includes(j)) {
            $td.attr("style", "min-width:100px;");
          } else {
            $td.attr("nowrap", "nowrap");

            if (j === "pricequoted") {
              $td.addClass("text-end");
            }
          }

          if (j === "email") {
            $td.empty();

            if (!!item[j]) {
              let $email = $(`<a />`, {
                href: `mailto:${item[j]}`,
                class: "text-decoration-none",
                html: `${i[j]}`,
              });

              $td.append($email);
            }
          }

          if (j === "contactnumber") {
            $td.empty();

            if (!!item[j]) {
              let $contactnumber = $(`<a />`, {
                href: `tel:${item[j]}`,
                class: "text-decoration-none",
                html: `${i[j]}`,
              });

              $td.append($contactnumber);
            }
          }

          $tr.append($td);
        });

        return $tr;
      },
      listview: function () {
        let list = this.list.map((item) => item);

        let $list = $("<tbody />");

        if (list.length > 0) {
          list.forEach((item) => {
            $list.append(this.row(item));
          });
        } else {
          $list.append(this.noRecordsFound());
        }

        this.$container.find("table tbody").replaceWith($list);
      },
      noRecordsFound: function () {
        return $("<tr />").append(
          $("<td />", { colspan: 8, class: "align-middle text-center" }).append(
            $("<div />", {
              class:
                "no-record-found padding-5 margin-auto text-center font-bold width-100",
              html: "No record has been found.",
            })
          )
        );
      },
      __bindListener: function () {
        let $rows = this.$container.find("table");

        let _this = this;

        $rows.on("click", 'tbody tr td button[name="extract"]', function (e) {
          let $button = $(e.target);

          let id = $button.val();

          let listitems = _this.list.map((item) => item);

          let item = listitems.filter((i) => i.id == id);

          PrepostEvent.emit("extract", item[0]);
        });
      },
      onExtract: function (callback) {
        PrepostEvent.subscribe("extract", callback);
      },
    };

    return PrepostListview;
  })();

  const PrepostPagination = (function () {
    let total_pages = 0;

    const $pagination = $("#__prepost-pagination");

    const __render = function (options) {
      options = Object.assign(
        {},
        {
          page: 1,
          pages: 0,
        },
        options
      );

      let page = Number(options.page) || 1;
      let pages = Number(options.pages) || 0;

      page = page < 1 || page > pages ? 1 : page;

      $("#__prepost-pagination-counter").html(`page ${page} of ${pages}`);

      $pagination.find("li a.pagination-link").each(function () {
        let pagedata = $(this).attr("data-page");

        if (pages > 1) {
          if (page === 1) {
            if (["first", "previous"].includes(pagedata)) {
              $(this).parent("li").addClass("disabled");
              $(this).removeAttr("href");
            } else {
              $(this).parent("li").removeClass("disabled");

              if ("next" === pagedata) {
                $(this).attr("href", `#page=${page + 1}`);
              } else if ("last" === pagedata) {
                $(this).attr("href", `#page=${pages}`);
              }
            }
          } else if (page === pages) {
            if (["last", "next"].includes(pagedata)) {
              $(this).parent("li").addClass("disabled");
              $(this).removeAttr("href");
            } else {
              $(this).parent("li").removeClass("disabled");

              if ("previous" === pagedata) {
                $(this).attr("href", `#page=${page - 1}`);
              } else if ("first" === pagedata) {
                $(this).attr("href", "#page=1");
              }
            }
          } else {
            $(this).parent("li").removeClass("disabled");

            if ("previous" === pagedata) {
              $(this).attr("href", `#page=${page - 1}`);
            } else if ("next" === pagedata) {
              $(this).attr("href", `#page=${page + 1}`);
            } else if ("first" === pagedata) {
              $(this).attr("href", "#page=1");
            } else if ("last" === pagedata) {
              $(this).attr("href", `#page=${pages}`);
            }
          }
        } else {
          if (!$(this).parent("li").hasClass("disabled")) {
            $(this).parent("li").addClass("disabled");
          }

          $(this).removeAttr("href");
        }
      });
    };

    const __bindListener = function () {
      $pagination.find("li a.pagination-link").each(function () {
        $(this).click(function (e) {
          e.stopPropagation();
          e.preventDefault();

          let page = $(this).attr("href");
          page = page.replace(/\D/g, "");

          page = page || 1;

          window.location.hash = `page=${page}`;

          __render({
            page: page,
            pages: total_pages,
          });

          PrepostEvent.emit("paginate", {
            page: page,
            pages: total_pages,
          });
        });
      });
    };

    __bindListener();

    return {
      paginate: function (page, pages) {
        __render({
          page: page,
          pages: pages,
        });
      },
      onPaginate: function (callback) {
        PrepostEvent.subscribe("paginate", callback);
      },
    };
  })();

  const PrepostSearch = (function () {
    const $search = $("#__prepost-search");

    let $key = $search.find("#__prepost-search-key");
    let $keyword = $search.find("#__prepost-search-input");
    let $btn = $search.find("#__prepost-search-button");

    const __search = function (key, keyword) {
      let page = 1;
      window.location.hash = `page=${page}`;
      PrepostEvent.emit("search", {
        key: key,
        keyword: keyword,
        page: 1,
      });
    };

    const __bindListener = function () {
      $key.on("change", function (e) {
        let key = $("option:selected", this).val();
        let keyword = $keyword.val();

        if (!!keyword) {
          if (["Filter By", "all"].includes(key)) {
            $keyword.val("");
          }

          $keyword.trigger("input");
        }
      });

      $keyword.on("input", function (e) {
        let key = $key.val();
        let keyword = $(this).val();

        __search(key, keyword);
      });

      $btn.on("click", function (e) {
        let key = $key.val();
        let keyword = $keyword.val();

        __search(key, keyword);
      });
    };

    __bindListener();

    return {
      searchData: function () {
        let __key = $("option:selected", $key).val();
        let __keyword = $keyword.val();

        return {
          key: __key,
          keyword: __keyword,
        };
      },
      onSearch: function (callback) {
        PrepostEvent.subscribe("search", callback);
      },
    };
  })();

  const PrepostExtract = (function () {
    function PrepostExtract(item) {
      this.item = Object.assign(
        {},
        {
          cleaningdate: "",
          firstlastname: "",
          address: "",
          postcode: "",
          appliancetype: "",
          pricequoted: 0,
          contactnumber: "",
          email: "",
          condition: "",
          needsattention: "",
          invoice_amount: 0,
        },
        item
      );

      this.$container = PrepostConfig.get("extractpage");

      this.$mainpage = PrepostConfig.get("mainpage");

      this.__bindListener();
    }

    PrepostExtract.prototype = {
      getForm: function () {
        let id = Math.random().toString(36).slice(2);

        let $formContainer = $("<div />", {
          id: "__prepost-modal-form",
          class: "prepost-container border border-light padding-3",
          style: "max-width: 800px;",
        });

        let $form = $("<form />", {
          action: "#",
          method: "post",
          role: "form",
        });

        //Field Customer information
        let $fieldsetOne = $("<fieldset />", {
          class: "prepost-fieldset margin-bottom-3",
        }).append(
          $("<legend />", {
            class: "font-16 text-uppercase font-bolder border-bottom",
            html: `Customer's Information`,
          })
        );

        let $email = this.item.email
          ? $("<a />", {
              href: `mailto:${this.item.email}`,
              class: "text-decoration-none",
              html: `${this.item.email}`,
            })
          : $("<span />", {
              html: "&nbsp;",
            });

        let $phone = this.item.contactnumber
          ? $("<a />", {
              href: `tel:${this.item.contactnumber}`,
              class: "text-decoration-none",
              html: `${this.item.contactnumber}`,
            })
          : $("<span />", {
              html: "&nbsp;",
            });

        let $table = $("<table />", {
          class:
            "prepost-table prepost-table-striped prepost-table-hover prepost-table-bordered",
        }).append(
          $("<tbody />")
            .append(
              $("<tr />")
                .append(
                  $("<th />", {
                    class: "text-left",
                    nowrap: "nowrap",
                    style: "width:150px; max-width: 150px;",
                    html: "Cleaning Date",
                  })
                )
                .append(
                  $("<td />", {
                    nowrap: "nowrap",
                    html: `${this.item.bcleaningdate}`,
                  })
                )
            )
            .append(
              $("<tr />")
                .append(
                  $("<th />", {
                    class: "text-left",
                    nowrap: "nowrap",
                    html: "Customer",
                  })
                )
                .append(
                  $("<td />", {
                    nowrap: "nowrap",
                    html: `${this.item.firstlastname}`,
                  })
                )
            )
            .append(
              $("<tr />")
                .append(
                  $("<th />", {
                    class: "text-left",
                    nowrap: "nowrap",
                    html: "Email",
                  })
                )
                .append($("<td />", { nowrap: "nowrap" }).append($email))
            )
            .append(
              $("<tr />")
                .append(
                  $("<th />", {
                    class: "text-left",
                    nowrap: "nowrap",
                    html: "Phone",
                  })
                )
                .append($("<td />", { nowrap: "nowrap" }).append($phone))
            )
            .append(
              $("<tr />")
                .append(
                  $("<th />", {
                    class: "text-left",
                    nowrap: "nowrap",
                    html: "Address",
                  })
                )
                .append(
                  $("<td />", {
                    nowrap: "nowrap",
                    html: `${this.item.address} ${this.item.postcode}`,
                  })
                )
            )
            .append(
              $("<tr />")
                .append(
                  $("<th />", {
                    class: "text-left",
                    nowrap: "nowrap",
                    html: "Appliance(s)",
                  })
                )
                .append(
                  $("<td />", {
                    nowrap: "nowrap",
                    html: `${this.item.appliancetype}`,
                  })
                )
            )
            .append(
              $("<tr />")
                .append(
                  $("<th />", {
                    class: "text-left",
                    nowrap: "nowrap",
                    html: "Quoted Price",
                  })
                )
                .append(
                  $("<td />", {
                    nowrap: "nowrap",
                    html: `${this.item.pricequoted}`,
                  })
                )
            )
        );

        $fieldsetOne.append($table);

        $form
          .append($fieldsetOne)

          // Field appliance condition
          .append(
            $("<fieldset />", {
              class: "prepost-fieldset margin-bottom-3",
              id: "appliancecondition",
            })
              .append(
                $("<legend />", {
                  class: "font-16 text-uppercase font-bolder border-bottom",
                  html: `Appliance Condition`,
                })
              )
              .append(
                $("<textarea />", {
                  class: "prepost_condition prepost-textarea width-100",
                  name: "condition",
                  style: "height: 150px;",
                  placeholder: "Enter text here. . .",
                })
              )
          )

          // Field need attention
          .append(
            $("<fieldset />", {
              class: "prepost-fieldset margin-bottom-3",
              id: "needattention",
            })
              .append(
                $("<legend />", {
                  class: "font-16 text-uppercase font-bolder border-bottom",
                  html: `Needs Attention`,
                })
              )
              .append(
                $("<textarea />", {
                  class: "prepost_needattention prepost-textarea width-100",
                  name: "needsattention",
                  style: "height: 150px;",
                  placeholder: "Enter text here. . .",
                })
              )
          )

          // Field invoice amount
          .append(
            $("<fieldset />", {
              class: "prepost-fieldset margin-bottom-3",
            })
              .append(
                $("<legend />", {
                  class: "font-16 text-uppercase font-bolder border-bottom",
                  html: `Invoice Amount (<span style="text-transform: none;">inc GST</span>)`,
                })
              )
              .append(
                $("<div />", {
                  class: "prepost-input-group text-right",
                }).append(
                  $("<input />", {
                    type: "number",
                    min: "0",
                    name: "invoice_amount",
                    id: "invoice_amount",
                    class: "prepost-input width-50 width-sm-100",
                    placeholder: "0.00",
                    style: "text-align: right;",
                    "aria-label": "Amount",
                  })
                )
              )
          )

          // Submit / Reset Button
          .append(
            $("<div />", {
              class: "text-right border-top border-light padding-top-2",
            })
              .append(
                $("<button />", {
                  type: "button",
                  name: "extract",
                  class: "prepost-btn prepost-btn-success",
                  html: `Click / Tap to Sign Document`,
                })
              )
              .append(
                $("<button />", {
                  type: "reset",
                  name: "reset",
                  class: "prepost-btn prepost-btn-secondary margin-left-2",
                  html: "Cancel",
                })
              )
          );

        $formContainer.append($form);

        this.$container.empty().append($formContainer);
      },
      send: async function (item) {
        this.setLoadingButton(true);

        let formData = new FormData();

        formData.append("customer", item.firstlastname);
        formData.append("email", item.email);
        formData.append("phone", item.contactnumber);
        formData.append("address", item.address + " " + item.postcode);
        formData.append("appliance", item.appliancetype);
        formData.append("cleaningdate", item.cleaningdate);
        formData.append("condition", item.condition);
        formData.append("need_attention", item.needsattention);
        formData.append("amount", item.invoice_amount);

        let rand = Math.random()
          .toString(36)
          .replace(/[^a-z]+/g, "")
          .substr(0, 5);

        let nonce = `${PrepostConfig.get("nonce")}`;

        await axios({
          method: "post",
          url: `${PrepostConfig.get("base_url")}/prepostieghae6a/pdf${rand}`,
          headers: {
            "Content-Type": "multipart/form-data",
            "X-WP-Nonce": nonce,
          },
          data: formData,
          responseType: "blob",
        }).then((xhr) => {
          let data = xhr.data;
          let dataType = data.type;

          let contentDesposition = xhr.headers["content-disposition"];

          let filename = "";

          if (
            contentDesposition &&
            contentDesposition.indexOf("attachment") !== -1
          ) {
            let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            let matches = filenameRegex.exec(contentDesposition);
            if (matches != null && matches[1])
              filename = matches[1].replace(/['"]/g, "");
          }

          let blob = new Blob([data], {
            type: dataType,
          });

          const URL = window.URL || window.webkitURL;
          const fileurl = URL.createObjectURL(blob);

          window.open(fileurl, "_system");

          setTimeout(function () {
            URL.revokeObjectURL(fileurl);
          }, 100);

          this.setLoadingButton(false);
        });
      },
      setLoadingButton: function (status) {
        let $btn = this.$container.find(
          'button[type="button"][name="extract"]'
        );

        if (!!status) {
          $btn.addClass("disabled").html("Generating PDF File...");
        } else {
          $btn.removeClass("disabled").html(`Click / Tap to Sign Document`);
        }
      },
      __bindListener: function () {
        const _this = this;

        this.$container.on(
          "click",
          'button[type="button"][name="extract"]',
          function (e) {
            const $form = $(e.target).closest("form");

            const data = $form.serializeArray().reduce(function (obj, item) {
              obj[item.name] = item.value;
              return obj;
            }, {});

            const senddata = Object.assign(
              {},
              {
                firstlastname: _this.item.firstlastname,
                email: _this.item.email,
                contactnumber: _this.item.contactnumber,
                address: _this.item.address,
                postcode: _this.item.postcode,
                appliancetype: _this.item.appliancetype,
                cleaningdate: _this.item.cleaningdate,
                condition: "",
                needsattention: "",
                invoice_amount: "",
              },
              data
            );

            _this.send(senddata);
          }
        );

        this.$container.on(
          "click",
          'button[type="reset"][name="reset"]',
          function (e) {
            _this.$container.hide();
            _this.$mainpage.show();
            _this.$container.empty();
          }
        );
      },
    };

    return PrepostExtract;
  })();

  const Prepost = (function () {
    function Prepost(options) {
      this.config = Object.assign(
        {},
        {
          containerElem: "#__prepost-plugin-app",
          rows_per_page: 10,
          spreadsheet: null,
          title: null,
          base_url: null,
          base_api_url: null,
          nonce: null,
        },
        options
      );

      this.$container = $(this.config.containerElem);

      this.config.spreadsheet = this.$container.attr("data-spreadsheet");
      this.config.title = this.$container.attr("data-title");

      this.$mainpage = this.$container.find("#__prepost-mainpage");
      this.$extractpage = this.$container.find("#__prepost-extractpage");

      this.$header = this.$mainpage.find("#__prepost-plugin-header");
      this.$main = this.$mainpage.find("#__prepost-plugin-main");
      this.$footer = this.$mainpage.find("#__prepost-plugin-footer");

      this.config.mainpage = this.$mainpage;
      this.config.extractpage = this.$extractpage;
      this.config.header = this.$header;
      this.config.main = this.$main;
      this.config.footer = this.$footer;

      PrepostConfig.config(this.config);

      this.$extractpage.hide();
      this.$mainpage.show();

      this.prepostRecord = new PrepostRecord({
        rows_per_page: this.config.rows_per_page,
      });

      this.prepostListview = new PrepostListview(this.$main);

      this.init();
    }

    Prepost.prototype = {
      init: async function () {
        this.setLoading();

        await this.prepostRecord.fetch({
          spreadsheet: this.config.spreadsheet,
        });

        this.setTotalQuotedAmount();

        let page = 1;

        if (window.location.hash) {
          let hash = window.location.hash.substring(1);
          page = hash.replace(/\D/g, "");

          page = page || 1;
        }

        this.generateView({
          page: page,
        });

        this.__bindListener();
      },
      generateView: function (options) {
        const _this = this;

        this.$header.find(".prepost-plugin-page-title").each(function () {
          $(this).html(_this.config.title);
        });

        options = Object.assign(
          {},
          {
            search: null,
            page: 1,
            perpage: 0,
          },
          options
        );

        let search = options.search || null;

        let page = options.page || 1;

        let perpage = options.perpage || this.config.rows_per_page;

        let data = this.prepostRecord.fetchRecords(search, page, perpage);

        this.prepostListview.setList(data.records);

        PrepostPagination.paginate(data.page, data.pages);

        this.prepostListview.listview();
      },
      handlePagination: function (data) {
        let search = PrepostSearch.searchData();
        this.generateView({
          search: search,
          page: data.page,
        });
      },
      handleSearch: function (data) {
        this.generateView({
          search: data,
          page: 1,
        });
      },
      handleExtract: function (item) {
        const prepostExtract = new PrepostExtract(item);
        prepostExtract.getForm();

        this.$mainpage.hide();
        this.$extractpage.show();
      },
      setLoading: function () {
        this.$main
          .find("table tbody")
          .empty()
          .append(
            $("<tr />").append(
              $("<td />", {
                colspan: 8,
                class: "align-middle text-center",
              }).append(
                $("<div />", {
                  class:
                    "loading padding-5 margin-auto text-center font-bold width-100",
                  html: "Loading Prepost Records...",
                })
              )
            )
          );
      },
      setTotalQuotedAmount: function () {
        let quotedAmount = this.prepostRecord.getTotalQuotation();

        $("#__prepost-total-quotation-amount").html(
          accounting.formatMoney(quotedAmount)
        );
      },
      __bindListener: function () {
        const _this = this;

        PrepostPagination.onPaginate(function (data) {
          _this.handlePagination(data);
        });

        PrepostSearch.onSearch(function (data) {
          _this.handleSearch(data);
        });

        this.prepostListview.onExtract(function (data) {
          _this.handleExtract(data);
        });
      },
    };

    return Prepost;
  })();

  const app = new Prepost({
    containerElem: "#__prepost-plugin-app",
    rows_per_page: 10,
    base_url: wp_prepost_rest_api.base_url,
    base_api_url: wp_prepost_rest_api.base_api_url,
    nonce: wp_prepost_rest_api.nonce,
  });
})(jQuery, window);
